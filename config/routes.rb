Rails.application.routes.draw do
  match "/404", to: "errors#not_found", via: :all
  match "/422", to: "errors#unprocessable", via: :all
  match "/500", to: "errors#internal_server_error", via: :all

  get 'home', to: 'home#show'
  get 'help', to: 'help#show'

  get '/users/me', to: 'users#me', as: 'me'

  get 'login', to: redirect('/auth/google'), as: 'login'
  get '/auth/google/callback', to: 'sessions#create'
  get 'logout', to: 'sessions#destroy', as: 'logout'

  get 'search', to: 'search#index'
  get 'search/results', to: 'search#show'

  get 'filter_sections_by_course', to: "courses#filter_sections_by_course"

  resources :users
  resources :courses
  resources :sections
  resources :timeslots

  root to: "home#show"
end
