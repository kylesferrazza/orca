# Orca TODO
- [x] faculty needs to be able to sign in (hd: "northeastern.edu")
- [x] users should be anonymous until partnered
- [x] only show users in my section, always
- [ ] match with someone (mutual), reveal name, face, picture
- [ ] all matches are final for that partner rotation
- [ ] quadrants of campus - NOT dorms
  - [x] remove dorms
  - [ ] add quadrants
- [ ] user availability as a search criteria
  - [x] TimeSlot
  - [x] user has_many timeslots
  - [x] timeslot belongs_to user
  - [x] display user availability percentage in search#show
  - [ ] show more precise availability under show more...?
  - [x] allow editing availability in user#edit
