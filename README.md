# <img src="app/assets/images/orca.svg" width="auto" height="40px" alt="Orca">&nbsp;[Orca Partner Finder][orca]

Orca is a web application that will help you meet a partner to work with for CS2500 or CS2510 at Northeastern University.

Important information:

* Orca has only been tested to work with Ruby 2.4.1

* Make sure to run `bundle install --without production` if you are trying to run orca locally. This will cause bundler to install sqlite instead of postgres.

* `rails db:setup` should properly set up the database for the first time.


* Orca expects the following environment variables to be present:
  * `GOOGLE_CLIENT_ID`
  * `GOOGLE_CLIENT_SECRET`

[orca]: https://orca.kylesferrazza.com/
