require 'test_helper'

class TimeslotTest < ActiveSupport::TestCase

  test "valid timeslot" do
    assert timeslots(:good).valid?
  end

  test "overlapping timeslot is invalid" do
    refute timeslots(:overlap).valid?, "overlapping timeslot is valid"
  end

  test "wrong week time is invalid" do
    refute timeslots(:future).valid?, "timeslot is valid in the wrong week"
  end

  test "end_time before start_time is invalid" do
    refute timeslots(:reversed).valid?, "timeslot is valid with end_time before start_time"
  end
end
