require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "invalid emails" do
    refute users(:johngmail).valid?
    assert users(:johngmail).errors.full_messages.include?("Email must be an @husky.neu.edu or @northeastern.edu address")
  end

  test "invalid section-course pair" do
    refute users(:bad).valid?
    assert users(:bad).errors.full_messages.include?("Section must be in selected course.")
  end

  test "picture_set?" do
    refute users(:johngmail).picture_set?
    assert users(:johnhusky).picture_set?
  end
end
