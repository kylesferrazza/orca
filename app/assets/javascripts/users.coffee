$ ->
  $("#user_course_id").on "change", ->
    selectedCourse = $("#user_course_id").val()
    console.log "filtering to " + selectedCourse
    $.ajax
      url: "/filter_sections_by_course"
      type: "GET"
      data: { selected_course: $("#user_course_id").val() }
