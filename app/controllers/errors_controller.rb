class ErrorsController < ApplicationController
  def not_found
    render status: 404
  end

  def unprocessable
    render status: 422
  end

  def internal_server_error
    render status: 500
  end

  def bad_domain
    redirect_to root_path, flash: {error: "In order to use Orca, you must sign in with an @husky.neu.edu or @northeastern.edu account."}
  end
end
