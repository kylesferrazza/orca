class SectionsController < ApplicationController
  before_action :authenticate
  before_action :require_admin, only: "index"

  def index
    @sections = Section.all
  end

  def show
    @section = Section.where(id: params[:id]).first
    return if @section.nil?
    redirect_to @section.registrar_link
  end
end
