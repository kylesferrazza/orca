class TimeslotsController < ApplicationController
  before_action :authenticate

  def index
    if current_user.admin?
      @timeslots = Timeslot.all
    else
      @timeslots = current_user.timeslots
    end
  end

  def new
  end

  def create
    timeslot_params = params.require(:timeslot).permit(:day, :start_hour, :start_minute, :end_hour, :end_minute)
    timeslot_params[:user] = current_user
    @timeslot = Timeslot.create(timeslot_params)
    if @timeslot.save
      redirect_to timeslots_path
    else
      redirect_back fallback_location: new_timeslot_path, flash: {error: @timeslot.errors.full_messages.to_sentence}
    end
  end

  def destroy
    @timeslot = Timeslot.find(params[:id])
    if @timeslot.user.id != current_user.id and !current_user.admin?
      redirect_back(fallback_location: root_path, flash: {error: "You do not have permission to access that page."})
      return
    end
    @timeslot.destroy
    redirect_to timeslots_path
  end
end
