class UsersController < ApplicationController
  before_action :authenticate
  before_action :require_admin, only: "index"

  def index
    @users = User.all
  end

  def show
    begin
      @user = User.find params[:id]
    rescue
      @user = nil
    end
    if @user.nil? or @user.id != current_user.id and !current_user.admin?
      redirect_back(fallback_location: current_user, flash: {error: "You do not have permission to access that page."})
      return
    end
  end

  def edit
    if current_user.id.to_s != params[:id] and !current_user.admin?
      byebug
      redirect_back(fallback_location: current_user, flash: {error: "You do not have permission to access that page."})
      return
    end
    @user = User.where(id: params[:id]).first
  end

  def update
    new_values = params.require(:user).permit(:course_id, :section_id, :searching)
    @user = User.where(id: params[:id]).first
    if @user.id != current_user.id and !current_user.admin?
      redirect_back fallback_location: root_path, flash: {error: "You do not have permission to access that page."}
      return
    end
    if @user.update_attributes(new_values)
      redirect_to @user, flash: {notice: "Successfully updated profile."}
    else
      redirect_back fallback_location: @user, flash: {error: @user.errors.full_messages.to_sentence}
    end
  end

  def me
    redirect_to current_user
  end

end
