class CoursesController < ApplicationController
  before_action :authenticate
  before_action :require_admin, only: "index"

  def index
    @courses = Course.all
  end

  def show
    @course = Course.where(id: params[:id]).first
    redirect_to "https://course.ccs.neu.edu/#{@course.name.downcase}"
  end

  def filter_sections_by_course
    @filtered_sections = Section.where(course_id: params[:selected_course])
  end
end
