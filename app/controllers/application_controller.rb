class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user
  helper_method :user_signed_in?

  def authenticate
    session[:next] = request.fullpath
    redirect_to :login unless user_signed_in?
  end

  def current_user
    @current_user ||=User.find(session[:user_id]) if session[:user_id]
  end

  def user_signed_in?
    !!current_user
  end

  def require_admin
    unless current_user.admin?
      redirect_back fallback_location: root_path, flash: { error: "You do not have access to that page." }
      return
    end
  end

end
