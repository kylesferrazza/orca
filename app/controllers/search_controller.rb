class SearchController < ApplicationController
  before_action :authenticate

  def index
  end

  def show
    search_fields = params.permit()

    @users = User.all
    # @users = @users.where(course_id: current_user.course_id)
    @users = @users.where(section_id: current_user.section_id)
    @users = @users.where(searching: true)
    @users = @users.where.not(id: current_user.id)

    if @users.empty?
      redirect_back fallback_location: search_path, flash: {error: "No matching users found."}
    end
  end
end
