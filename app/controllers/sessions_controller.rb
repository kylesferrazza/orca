class SessionsController < ApplicationController

  def create
    user = User.find_or_create_from_auth_hash(request.env["omniauth.auth"])
    session[:user_id] = user.id
    if current_user.section.crn == 0
      redirect_to edit_user_path(current_user), flash: {notice: "Signed in successfully. Please choose a course and section to begin searching for a partner."}
      return
    end
    redirect_to session[:next] || me_path, flash: {notice: "Signed in successfully."}
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_path, flash: {notice: "Signed out successfully."}
  end

end
