class Section < ApplicationRecord
  has_many :users
  belongs_to :course

  def link_or_unspec
    crn.equal?(0) ? "Unspecified" : ActionController::Base.helpers.link_to("#{crn}: #{description}", Rails.application.routes.url_helpers.section_path(self))
  end

  def registrar_link
    "https://wl11gp.neu.edu/udcprod8/bwckschd.p_disp_detail_sched?term_in=201830&crn_in=#{crn}"
  end

  def desc
    if crn == 0
      description
    else
      "#{crn}: #{description}"
    end
  end

end
