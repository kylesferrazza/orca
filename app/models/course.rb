class Course < ApplicationRecord
  has_many :users
  has_many :sections

  def link_or_unspec
    name == "Unspecified" ? "Unspecified" : ActionController::Base.helpers.link_to(name, Rails.application.routes.url_helpers.course_path(self))
  end
end
