class User < ApplicationRecord
  belongs_to :course
  belongs_to :section
  has_many :timeslots

  validates :email, format: { with: /\A.*@(husky\.neu|northeastern)\.edu\z/, message: "must be an @husky.neu.edu or @northeastern.edu address"}
  validates :course_id, :section_id, presence: true
  validate :section_matches_course

  def section_matches_course
    if section.course != course
      errors.add(:section, "must be in selected course.")
    end
  end

  def self.find_or_create_from_auth_hash(auth)
    where(uid: auth.uid).first_or_initialize.tap do |user|
      user.uid = auth.uid
      user.first_name = auth.info.first_name
      user.last_name = auth.info.last_name
      user.email = auth.info.email
      user.picture = auth.info.image
      user.course ||= Course.where(name: "Unspecified").first
      user.section ||= user.course.sections.first
      user.searching = true if user.searching.nil?
      user.save!
    end
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def profile_photo(size)
    ActionController::Base.helpers.image_tag picture, alt: "#{full_name}'s profile photo", id: "propic", size: size
  end

  def picture_set?
    picture != "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/c/photo.jpg"
  end

  def availability_match(other_user)
    sum = 0
    timeslots.each do |mine|
      other_user.timeslots.each do |theirs|
        sum += mine.overlap_hours(theirs)
      end
    end
    return "#{sum} hours"
  end
end
