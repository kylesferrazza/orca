class Timeslot < ApplicationRecord
  belongs_to :user

  validate :hours_valid
  validate :minutes_valid
  validate :in_correct_week
  validate :does_not_cross_day
  validate :start_before_end
  validate :timeslots_dont_overlap

  def self.display_hours
    pad_range(0..23)
  end

  def self.display_minutes
    pad_range(0..59)
  end

  def self.pad_range(r)
    r.map do |item|
      "%02d" % item
    end
  end

  def hours_valid
    unless start_hour >= 0 and start_hour <= 23
      errors.add(:start_hour, "must be between 0 and 23")
    end
    unless end_hour >= 0 and end_hour <= 23
      errors.add(:end_hour, "must be between 0 and 23")
    end
  end

  def minutes_valid
    unless start_minute >= 0 and start_minute <= 59
      errors.add(:start_minute, "must be between 0 and 59")
    end
    unless end_minute >= 0 and end_minute <= 59
      errors.add(:end_minute, "must be between 0 and 59")
    end
  end

  def start_time
    return DateTime.new(2018, 04, day, start_hour, start_minute)
  end

  def end_time
    return DateTime.new(2018, 04, day, end_hour, end_minute)
  end

  def in_correct_week
    unless start_time >= Timeslot.min_start_date
      errors.add(:start_time, "must be between #{Timeslot.min_start_date} and #{Timeslot.max_end_date}")
    end
    unless end_time <= Timeslot.max_end_date
      errors.add(:end_time, "must be between #{Timeslot.min_start_date} and #{Timeslot.max_end_date}")
    end
  end

  def does_not_cross_day
    if start_time.day != end_time.day
      errors.add(:start_time, "must be on same day as end time")
    end
  end

  def start_before_end
    unless start_time < end_time
      errors.add(:start_time, "must be before end time")
    end
  end

  def timeslots_dont_overlap
    user.timeslots.each do |other|
      if overlaps? other
        errors.add(:timeslots, "must not overlap")
      end
    end
  end

  def self.min_start_date
    DateTime.new(2018, 4, 1, 0, 0)
  end
  
  def self.max_end_date
    DateTime.new(2018, 4, 7, 23, 59)
  end

  def overlaps?(other)
    return false if id == other.id 
    start_time <= other.end_time and other.start_time <= end_time
  end

  def overlap_hours(other)
    return 0 if !overlaps?(other) 
    later_start = start_time > other.start_time ? start_time : other.start_time
    earlier_end = end_time < other.end_time ? end_time : other.end_time
    return ((earlier_end - later_start) * 24).to_f # difference in days by default
  end

  def day_of_week
    start_time.strftime("%A")
  end

  def time
    start_time.strftime("%l:%M%P") + " to " + end_time.strftime("%l:%M%P")
  end

  def desc
    "#{day_of_week}s, #{time}"
  end
end
