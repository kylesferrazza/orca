# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

courses = ["Unspecified", "CS2500", "CS2510"]

existing_courses = Course.all.map &:name

for course in courses
  Course.create(name: course) unless existing_courses.include?(course)
end


sections = [["Unspecified", 0, "Unspecified"],

            # FUNDIES 1
            ["CS2500", 30361, "TA: Ameen Radwan"],
            ["CS2500", 31931, "TA: Nadine Shaalan"],
            ["CS2500", 32746, "TA: Claudia Vilcu"],
            ["CS2500", 33615, "TA: Suzanne Becker"],
            ["CS2500", 35932, "TA: Julian Zucker"],
            ["CS2500", 35933, "TA: Andrew Cobb"],

            # FUNDIES 2
            ["CS2510", 30302, "TA: Christian Munoz-Robayo, WVH 108"],
            ["CS2510", 30741, "TA: DJ Richardson, DG 119"],
            ["CS2510", 30300, "TA: Manas Purohit, WVH 210"],
            ["CS2510", 31806, "TA: Jonathan Schuster, WVH 212"],
            ["CS2510", 30301, "EV 002"],
            ["CS2510", 32069, "TA: Kathryn Stavish, WVH 210"],
            ["CS2510", 33616, "WVH 212"],
            ["CS2510", 33617, "WVH 210"],
            ["CS2510", 35184, "WVH 212"],
            ["CS2510", 35185, "WVH 210"],
            ["CS2510", 37072, "WVH 212"],
            ["CS2510", 37833, "SN 015"]]

existing_sections = Section.all.map &:crn

for section in sections
  course_name = section[0]
  crn = section[1]
  description = section[2]
  course_id = Course.where(name: course_name).first.id
  if existing_sections.include?(crn)
    section = Section.where(crn: crn).first
    section.update_attributes description: description, course_id: course_id
  else
    Section.create(crn: crn, description: description, course_id: course_id)
  end
end
